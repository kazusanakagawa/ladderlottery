package ladderLottery;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * 2018/11/25
 * 
 * @author KazusaNakagawa Choice classで選択した結果から、どのアルファベットに行き着くか選択させる。
 * 
 */
public class Answer extends Choice {

	@Override
	int select(int result) {

		System.out.println("どのアルファベットに行き着くか答えて下さい。");

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		try {
			while (true) {
				String line = reader.readLine();

				// 文字列が 'ブランク' の時のエラーを強制的に回避 '0' が入力される。
				if (line.length() > 0) {
					setChoice(line.charAt(0));
				} else {
					setChoice('0');
				}
				// 選択結果
				switch (getChoice()) {
				case 'A':
				case 'a':
					System.out.println("Aですね。");
					break;
				case 'B':
				case 'b':
					System.out.println("Bですね。");
					break;
				case 'C':
				case 'c':
					System.out.println("Cですね。");
					break;
				case 'D':
				case 'd':
					System.out.println("Dですね。");
					break;
				case 'E':
				case 'e':
					System.out.println("Eですね。");
					break;
				default:
					System.out.println("A から Eで選択して下さい。");
					continue;
				}
				break;
			} // while
		} catch (StringIndexOutOfBoundsException e) {
			System.out.println("空enter");
			System.out.println(e);
			System.out.println("上書き状態になりました。");
		} catch (Exception e) {
			System.out.println(e);
		}
		return getChoice();
	}

}
