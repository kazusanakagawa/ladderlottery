package ladderLottery;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * 2018/11/13
 * 
 * @author KazusaNakagawa 選択//結果表示
 *
 */

public class Choice {

	private char choice;// 選択する

	/**
	 * 選択したアルファベットの表示
	 * 
	 * @param result
	 * @return choice
	 */
	int select(int result) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		try {
			while (true) {
				String line = reader.readLine();

				// 文字列が 'ブランク' の時のエラーを強制的に回避 '0' が入力される。
				if (line.length() > 0) {
					setChoice(line.charAt(0));
				} else {
					setChoice('0');
				}
				// 選択結果
				switch (getChoice()) {
				case 'A':
				case 'a':
					System.out.println("Aですね。");
					break;
				case 'B':
				case 'b':
					System.out.println("Bですね。");
					break;

				case 'C':
				case 'c':
					System.out.println("Cですね。");
					break;

				case 'D':
				case 'd':
					System.out.println("Dですね。");
					break;

				case 'E':
				case 'e':
					System.out.println("Eですね。");
					break;

				default:
					System.out.println("A から Eで選択して下さい。");
					continue;
				}
				break;
			} // while

		} catch (StringIndexOutOfBoundsException e) {
			System.out.println("空enter");
			System.out.println(e);
			System.out.println("上書き状態になりました。");
		} catch (Exception e) {
			System.out.println(e);
		}
		return getChoice();

	}

	/**
	 * 
	 * @return
	 */
	public char getChoice() {
		return choice;
	}

	/**
	 * 
	 * @param choice
	 */
	public void setChoice(char choice) {
		this.choice = choice;
	}
}