package ladderLottery;

/**
 * 2018/11/28
 * @author KazusaNakagawa
 * 制限時間を実装
 *
 */

public class Timer {

	// 阿弥陀籤の難易度別、制限時間
	public static final int EASY = 1;
	public static final int NOMAL = 5;
	public static final int HARD = 10;
	public static final int VERY_HARD = 15;
	public static final int MANIAC = 50;
	
}
