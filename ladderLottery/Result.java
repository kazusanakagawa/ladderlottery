package ladderLottery;

/**
 * 2018/11/18
 * @author KazusaNakagawa
 * 結果表示
 */

public class Result  {

	public Result() {
		// TODO 自動生成されたコンストラクター・スタブ
	}
	
	/**
	 * 選択結果を表示
	 */
	void indicate() {
		
		//選択結果を呼び出してresultに代入
		int result= new Answer().select(0);
		 		
		// Aを選択した場合
		if (result== 'A' || result == 'a') {
			System.out.println("結果は 　F　 でした。");
		}
		// Bを選択した場合
		if (result == 'B' || result == 'b') {
			System.out.println("結果は 　D　 でした。");
		}
		// Cを選択した場合
		if (result == 'C' || result == 'c') {
			System.out.println("結果は 　B　 でした。");
		}
		// Dを選択した場合
		if (result == 'D' || result == 'd') {
			System.out.println("結果は 　E　 でした。");
		}
		// Eを選択した場合
		if (result == 'E' || result == 'e') {
			System.out.println("結果は 　C　 でした。");
		}
		// Fを選択した場合
		if (result == 'F' || result == 'f') {
			System.out.println("結果は 　A　 でした。");
		}
		System.out.println("");
	}
}
