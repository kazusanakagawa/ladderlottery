package ladderLottery;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;


/**
 * 2018/11/13
 * 
 * @author KazusaNakagawa 図面表示
 *
 */

public class Diagram {

	private int INPUT_NUMBER;// 表示個数を自分で入力

	

	public Diagram() {
		super();
		// TODO 自動生成されたコンストラクター・スタブ
	}

	

	/**
	 * 阿弥陀籤を何個表示させるか入力する
	 * 
	 * @param conut
	 * @return
	 */
	int input(int conut) {
		System.out.println("はじめに、阿弥陀籤を何個表示させますか。");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		while (true) {
			try {
				String line = reader.readLine();
				INPUT_NUMBER = Integer.parseInt(line);

				if (0 >= INPUT_NUMBER) {
					System.out.println("入力は１個以上で選択して下さい。");
					System.out.println("入力し直して！");
					continue;

				} else if (0 <= INPUT_NUMBER) {
					System.out.println(INPUT_NUMBER + "回ですね");
					break;

				} else {
					System.out.println("数字で入力して下さい。");
					continue;
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		} // while
		return INPUT_NUMBER;
	}

	/**
	 * 選択した個数分を表示
	 */
	void indicate() {

		// 選択した阿弥陀籤の難易度を呼び出す。
		int choice = new DegreeOfDifficulty().select(0);

		//System.out.println(choice + "回、阿弥陀籤を表示しました。");
		
		System.out.println("下記のアルファベットから好きなものを選択して下さい。");
		System.out.println("");

		String[] lottery = { "A", "B", "C", "D", "E", "F" };

		// 上段Alphabet表示
		for (String Alphabet : lottery) {
			System.out.print(Alphabet + "     ");
		}
		System.out.println("");

		for (int i = 0; i < choice; i++) {
			//System.out.println(i + 1 + "個目");

			ArrayList<String> branch = new ArrayList<String>();
			// 以下、阿弥陀のパターン追加
			branch.add("|ーー|　　|ーー|　　|ーー|");
			branch.add("|　　|ーー|　　|ーー|　　|");
			branch.add("|ーー|　　|ーー|　　|ーー|");
			branch.add("|　　|ーー|　　|ーー|　　|");
			branch.add("|　　|　　|ーー|　　|ーー|");
			branch.add("|ーー|　　|　　|ーー|　　|");

			// branchの並びを重複なしで混ぜる
			Collections.shuffle(branch);

			// 結果表示
			// System.out.println(branch);

			for (int in = 0; in < branch.size(); in++) {
				System.out.println(branch.get(in));
			}

		}
		// 下段Alphabet表示
		for (String Alphabet : lottery) {
			System.out.print(Alphabet + "     ");
		}
		System.out.println("");
	}
}
