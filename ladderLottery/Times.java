package ladderLottery;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Times {

	// 阿弥陀籤の回数を選択
	static final int CHOICE_NUMBER = 0;

	public static void main(String[] args) {

		choice();
	}

	static void choice() {
		System.out.println("何個表示させますか？");

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		while (true) {
			try {
				String line = reader.readLine();
				int n;				
				n = Integer.parseInt(line);
				if (0 >= n) {
					System.out.println("一個以上で選択して下さい。");
					System.out.println("入力し直して！");
					continue;
				} else if (n % 2 == 0) {
					System.out.println("2で割り切れてる");
				} else if (n % 3 == 0) {
					System.out.println("3で割り切れてる");
				} else if (n % 5 == 0) {
					System.out.println("5で割り切れてる");
				} else if (n % 7 == 0) {
					System.out.println("7で割り切れてる");
				} else {
					System.out.println(n + "は割り切れません。");
					System.out.println("割り切れる数字で再入力して下さい。");
					continue;
				}
				break;
			} catch (NumberFormatException e) {
				System.out.println(e);
				System.out.println("数字で入力して下さい。");
			} catch (Exception e) {
				System.out.println(e);
			}
		} // while
	}
}
