package ladderLottery;

/**
 * 2018/11/13
 * 
 * @author KazusaNakagawa
 * 
 *  阿弥陀籤を作成 実行するとランダムで線が生成される。 
 *  選択したアルファベットとと到達地点が一致するように実装する。
 *  また、籤を何回実行するかの選択も実装する。
 */

public class Main {

	public static void main(String[] args) {

		System.out.println("////////////////////////////////////////////////////////////////");
		System.out.println("///     ");
		System.out.println("///     阿弥陀籤をします。");
		System.out.println("///     選択したアルファベットから、どこに行き着くか考える。");
		System.out.println("///     ");
		System.out.println("////////////////////////////////////////////////////////////////");
		System.out.println("");
		

		// 図面表示
		new Diagram().indicate();

		// どのアルファベットを選択し開始するか。
		new Choice().select(0);

		// どのアルファベットに行き着くか選択//結果表示
		new Result().indicate();
		
		// 続けるか選択
		new Continuation().select();

	}
}
