package ladderLottery;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class DegreeOfDifficulty {

	// 阿弥陀籤の難易度を選択
	public static final int EASY = 1;
	public static final int NOMAL = 5;
	public static final int HARD = 10;
	public static final int VERY_HARD = 15;
	public static final int MANIAC = 50;

	/**
	 * 
	 * @param choice
	 * @return
	 */
	public int select(int choice) {
		System.out.println("＊＊＊　はじめに、難易度を選択( 半角数字入力 )　＊＊＊");

		String[] difficulty = { "1：EASY", "2：NOMAL", "3：HARD", "4：VERY_HARD", "5：MANIAC" };
		for (String display : difficulty) {
			System.out.println(display);
		}
		System.out.println("");

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		try {

			while (true) {
				String line = reader.readLine();
				char c = line.charAt(0);
				switch (c) {
				case '1':
					System.out.println("EASYを選択しました。");
					return EASY;

				case '2':
					System.out.println("NOMALを選択しました。");
					return NOMAL;

				case '3':
					System.out.println("EASYを選択しました。");
					return HARD;

				case '4':
					System.out.println("NOMALを選択しました。");
					return VERY_HARD;

				case '5':
					System.out.println("NOMALを選択しました。");
					return MANIAC;

				default:
					System.out.println("1 から ５でお願いします。");
					continue;
				}
			} // while
		} catch (StringIndexOutOfBoundsException e) {
			// TODO: handle exception
		} catch (Exception e) {
			System.out.println(e);
		}
		return choice;
	}
}
