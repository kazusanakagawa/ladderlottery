package ladderLottery;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * 2018/11/28
 * @author KazusaNakagawa
 * 続けてやるか
 *
 */
public class Continuation extends Choice {
	

	/**
	 *  続けて阿弥陀籤をするか。
	 * 
	 */ 
	void select() {
		
		System.out.println("続けますか。");
		System.out.println("1: 続ける　2 : 終了する");
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		try {
			while (true) {
				String line = reader.readLine();

				// 文字列が 'ブランク' の時のエラーを強制的に回避 '0' が入力される。
				if (line.length() > 0) {
					setChoice(line.charAt(0));
				} else {
					setChoice('0');
				}
				// 選択結果
				switch (getChoice()) {
				case '1':
					System.out.println("続けます。");
					System.out.println("");
					// 図面表示
					new Diagram().indicate();
					// どのアルファベットを選択し開始するか。
					new Choice().select(0);
					// どのアルファベットに行き着くか選択//結果表示
					new Result().indicate();
					//再度続けるか選択
					select();
					break;
					
				case '2':
					System.out.println("終了します。");
					break;

				default:
					System.out.println("1 から 2 で選択して下さい。");
					continue;
				}
				break;
			} // while

		} catch (StringIndexOutOfBoundsException e) {
			System.out.println("空enter");
			System.out.println(e);
			System.out.println("上書き状態になりました。");
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
